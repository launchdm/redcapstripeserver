<?php

    header('Access-Control-Allow-Origin: *');
    include './stripe-php/init.php';

    \Stripe\Stripe::setApiKey('sk_test_66YCqBtnam1rWHIKzhz0KZ7M');

    $firstName = $_POST["firstName"];
    $lastName = $_POST["lastName"];
    $phone = $_POST["phone"];
    $password = $_POST["password"];
    $email = $_POST["email"];
    $source = $_POST["source"];

    $metaData = array('name' => $firstName + ' ' + $lastName);
    $returnData = array('success' => false, 'message' => '', 'error' => '');

    try {

        \Stripe\Customer::create(array(
            "description" => "Customer for jacob.white@example.com",
            "email" => $email,
            "metadata" => $metaData,
            "source" => $source // obtained with Stripe.js
        ));
        
        $returnData['success'] = true;
        $returnData['message'] = "Registration successful.  Please sign in using the app.";

    } catch(\Stripe\Error\Card $e) {
        // Since it's a decline, \Stripe\Error\Card will be caught
        $body = $e->getJsonBody();
        $err  = $body['error'];
      
        print('Status is:' . $e->getHttpStatus() . "\n");
        print('Type is:' . $err['type'] . "\n");
        print('Code is:' . $err['code'] . "\n");
        // param is '' in this case
        print('Param is:' . $err['param'] . "\n");
        print('Message is:' . $err['message'] . "\n");

        $returnData['success'] = false;
        $returnData['message'] = $err['message'];
    } catch (\Stripe\Error\RateLimit $e) {
        // Too many requests made to the API too quickly
        $returnData['success'] = false;
        $returnData['message'] = 'RateLimit';
    } catch (\Stripe\Error\InvalidRequest $e) {
        // Invalid parameters were supplied to Stripe's API
        $returnData['success'] = false;
        $returnData['message'] = 'Invalid Request';

    } catch (\Stripe\Error\Authentication $e) {
        // Authentication with Stripe's API failed
        // (maybe you changed API keys recently)
        $returnData['success'] = false;
        $returnData['message'] = 'Authentication';

    } catch (\Stripe\Error\ApiConnection $e) {
        // Network communication with Stripe failed
        $returnData['success'] = false;
        $returnData['message'] = 'API Connection';

    } catch (\Stripe\Error\Base $e) {
        // Display a very generic error to the user, and maybe send
        // yourself an email
        $returnData['success'] = false;
        $returnData['message'] = 'Base';

    } catch (Exception $e) {
        // Something else happened, completely unrelated to Stripe
        $returnData['success'] = false;
        $returnData['error'] = $e->getMessage();
        $returnData['message'] = $e->getMessage();

    }


    echo json_encode($returnData, true);

    /*
    $post = array('firstName'=> 'Chris', 'lastName' => 'Williams', 'email' => 'c@c.com', 'password' => 'testtest', 'phone' => '+14125555555', 'web' => '1');
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,"https://api.dashride.com/v1.1/signup");
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($post));  //Post Fields
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $headers = ['dashride-api-key: 890b810e9e569317f81d391f713fbf4a1245c9a0648b00cece444fe62df633e994', 'Access-Control-Allow-Origin: *'];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $response = curl_exec($ch);
    */

?>